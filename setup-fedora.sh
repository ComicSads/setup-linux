#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	printf 'script needs to be run as the root user\n'
	printf 'run "sudo %s"\n' "$0"
	exit 1
fi

# Setup rpmfusion free and nonfree repos
dnf install "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"

# Setup automatic btrfs snapshots
#shellcheck disable=SC2016
printf '%s\n%s' '#!/bin/sh' 'btrfs subvolume snapshot -r /home "/snapshots/home-$(date --iso-8601)"' > /etc/cron.daily/snapshot-home.sh
chmod +x /etc/cron.daily/snapshot-home.sh
# Setup automatic btrfs scrubbing
#printf '%s\n%s' '#!/bin/sh' 'btrfs scrub start /' > /etc/cron.monthly/btrfs-scrub-root.sh
#chmod +x /etc/cron.monthly/btrfs-scrub-root.sh

# Add flathub
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Check if running nvidia
if lspci | grep VGA | grep -iq nvidia; then
	printf "Running with an Nvidia GPU, suggest installing nonfree drivers"
fi

printf 'please run "sudo dnf upgrade"'
